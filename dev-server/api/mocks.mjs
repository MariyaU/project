import express from 'express';
import { isDefined } from '../utils';

const router = express.Router();

const getData = async (path, { id }) => {
    const [path_, id_] = path.split(`/${id}`);
    if (isDefined(id_)) {
        const data = await import(`./data/${path_}/id.json`);
        return data.default;
    }
    const data = await import(`./data/${path_}/index.json`);
    return data.default;
};

let CARDS = [{ title: 'JS Course for everybody', name:'JS', author: 'Harry Potter', rating: '5.0', year: 1999, members: '4.52K'},
            { title: 'Python Course for woman', name:'Python', author: 'Vanya Pyp', rating: '4.7', year: 2000, members: '1.5K'}];

export default () => {
    router.get('/user', async (req, res) => {
        const data = await getData(req.path, req.params);
        return res.send(data);
    });

    router.get('/api/cards', async (req, res) => {

        return res.send({ cards: CARDS });
    });
    router.post('/api/card', async (req, res) => {
        const id = CARDS[(CARDS.length - 1)].id + 1; // generate id
        const card = { title: req.body.title, description: req.body.description, id };
        CARDS.push(card);
        return res.send({ card });
    });
    router.delete('/api/card', async (req, res) => {
        const cards = Array.from(CARDS);
        const deletedId = Number(req.query.id);

        const index = cards.reduce((acc, curr, index) => {
            if (curr.id === deletedId) {
                return index;
            }

            return acc;
        }, -1);
        const [deleted] = cards.splice(index, 1);
        CARDS = cards;
        return res.send(deleted);
    });
    return router;
};
