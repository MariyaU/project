module.exports = {
    root: true,
    extends: ['airbnb-typescript'],
    parser: "typescript-eslint-parser",
    parserOptions: {
        "jsx": true,
        "useJSXTextNode": true
    },
    plugins: ["typescript"],
    end: {
        "browser": true,
        "window":true,
        "node": true,
        "jasmine": true
    },

    rules: {
        "react/jsx-fragments": 'off',
        'import/no-extraneous-dependencies':'off',
        'import/order': 'off',
        'object-curly-newline' : 'off',
        'arrow-body-style' : 'off',
        'jsx-quotes': [1, "prefer-single"],
        'eol-last': 'off',
        "indent": "off",
        'react/jsx-indent': ["error", 4],
        'react/jsx-indent-props': ["error", 4],
        "@typescript-eslint/indent": ["error", 4],
        'guard-for-in': 0,
        'react/prop-types': 0,
        'no-unused-vars': 'error',
        'comma-dangle': [2, 'always-multiline'],
        'typescript/no-unused-vars': 'error',
        'react/jsx-closing-bracket-location': [2, 'tag-aligned'],
    },
    settings: {
        react: {
            version: 'latest'
        }
    }
}
