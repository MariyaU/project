import * as React from 'react';
import { Route, Redirect, Switch } from 'react-router-dom';
import Cards from './containers/cards';
import Form from './components/forms/form';
import main from './components/layout/layout';

const CardsScreen = main(Cards);
const FormScreen = main(Form);

const Routes = () => (
    <Switch>
        <Route exact path='/' component={CardsScreen} />
        <Route exact path='/cards' component={CardsScreen} />
        <Route exact path='/form' component={FormScreen} />
        <Redirect to='/' />
    </Switch>
);

export default Routes;
