import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import { routerMiddleware } from 'react-router-redux';
import { createBrowserHistory } from 'history';
import reducers from '../reducers';

const configureStore = (initialState: {}, history: {}): any => {
    let composeEnhancers = compose;
    const middleware = [thunk, routerMiddleware(history)];

    if (process.env.NODE_ENV !== 'production') {
        if (window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) {
            composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({ name: 'Notifications' });
        }
    }

    const enhancer = composeEnhancers(applyMiddleware(...middleware));
    const store = createStore(reducers, initialState, enhancer);

    if (module.hot) {
        module.hot.accept('../reducers', () => {
            store.replaceReducer(require('../reducers').default);
        });
    }

    return store;
};

export {
    configureStore,
    createBrowserHistory,
};
