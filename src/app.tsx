import React from 'react';
import { Route, Router } from 'react-router-dom';
import { Provider } from 'react-redux';
import Routes from './routes';

const App: React.SFC<{ store, history }> = ({ store, history }): JSX.Element | null => {
    return (
        <Provider store={store}>
            <Router history={history}>
                <Route path='/' component={Routes} />
            </Router>
        </Provider>
    );
};

export default App;
