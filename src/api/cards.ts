
import rq from '../utils/request';

export const getCards = async (): Promise<any> => {
    try {
        const { data: { cards } } = await rq.get('api/cards');

        return cards;
    } catch (e) {
        throw e;
    }
};

export const addCard = async (title, description): Promise<any> => {
    try {
        const { data: { card } } = await rq.post('api/card', { title, description });

        return card;
    } catch (e) {
        throw e;
    }
};

export const delCard = async (id): Promise<any> => {
    try {
        const { data: card } = await rq.delete(`api/card?id=${id}`);

        return card;
    } catch (e) {
        throw e;
    }
};
