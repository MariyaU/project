/* eslint-env browser */
import React from 'react';
import ReactDOM from 'react-dom';
import App from './app';
import { BrowserRouter } from 'react-router-dom';
import { configureStore, createBrowserHistory } from './store/store';

const history = createBrowserHistory({});
const store = configureStore({}, history);

const render = (store, history) => {
    ReactDOM.render(
        (
            <BrowserRouter>
                <App store={store} history={history} />
            </BrowserRouter>
        ),
        document.getElementById('root'),
    );
};

render(store, history);

if (module.hot) {
    module.hot.accept('./app', () => {
        render(store, history);
    });
}
