const size = {
    mobileS: '540px',
    mobileM: '767px',
    tablet: '1023px',
    laptop: '1279px',
};

const devices = {
    mobileS: `(max-width: ${size.mobileS})`,
    mobileM: `(max-width: ${size.mobileM})`,
    tablet: `(max-width: ${size.tablet})`,
    laptop: `(max-width: ${size.laptop})`,
};

export default devices;
