import { Dispatch } from 'redux';
import * as api from '../api/cards';

export interface Action {
    type: string;
    payload: Card;
}
interface Card {
    title: string,
    name: string,
    author: string,
    rating: number | string,
    year: number,
    members: string,
}

export const addCard = (card: Card) => {
    return ({
        type: 'ADD_CARD',
        payload: card,
    }
    );
};

export const delCard = (card: Card) => {
    return ({
        type: 'DEL_CARD',
        payload: card,
    }
    );
};

export const setCards = (cards) => ({
    type: 'SET_CARDS',
    payload: cards,
} as const);

export const fetchCards = (): any => async (dispatch: Dispatch): Promise<any> => {
    try {
        const cards = await api.getCards();
        dispatch(setCards(cards));
    } catch (e) {
        throw e;
    }
};
export const pushCard = (title, description): any => async (dispatch: Dispatch): Promise<any> => {
    try {
        const card = await api.addCard(title, description);
        dispatch(addCard(card));
    } catch (e) {
        throw e;
    }
};
export const deleteCard = (id): any => async (dispatch: Dispatch): Promise<any> => {
    try {
        const card = await api.delCard(id);
        dispatch(delCard(card));
    } catch (e) {
        throw e;
    }
};
