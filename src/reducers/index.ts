import { combineReducers } from 'redux';
import { cards } from './cards';

const reducers = {
    cards,
};

export default combineReducers(reducers);
