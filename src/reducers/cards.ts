import { Action } from '../actions/cards';

export interface Card {
    title: string,
    name: string,
    author: string,
    rating: string,
    year: number,
    members: string
}

type State = Card[];

const initialState: State = [
    {
        title: 'JS Course for everybody',
        name: 'JS',
        author: 'Harry Potter',
        rating: '5.0',
        year: 1999,
        members: '4.52K',
    }
];

export function cards(state: State = initialState, action: Action) {
    switch (action.type) {
        case 'ADD_CARD': {
            const card = action.payload;
            return [
                ...state,
                card,
            ];
        }
        case 'SET_CARDS': {
            const cards = action.payload;
            return cards;
        }
        default:
            return state;
    }
}
