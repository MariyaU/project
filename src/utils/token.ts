const sessionStorage = window.sessionStorage;

export const checkToken = (): boolean => {
    const token = sessionStorage.getItem('token');
    if (token) {
        return true;
    }

    return false;
};

export const getToken = (): string | null => {
    const token = sessionStorage.getItem('token');

    return token ? token : null;
};

export const setToken = (token: string): void => {
    sessionStorage.setItem('token', token);
};

export const removeToken = () => {
    sessionStorage.removeItem('token');
    sessionStorage.removeItem('fullName');
};
