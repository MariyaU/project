import React from 'react';
import Card from '../components/card';
import { Grid, Col } from '../components/common/grids';
import { connect } from 'react-redux';
import { pushCard, fetchCards } from '../actions/cards';
import Topline from '../components/topline/topline';

const result = [
    {
        title: 'Name of course 1',
        author: 'author 1',
        year: 1999,
        members: '4.52K',
        mark: 4.53,
    },
    {
        id: '2',
        title: 'Name of course 2',
        author: 'author 2',
        year: 2000,
        members: '4.52K',
        mark: 4,
    },
    {
        id: '3',
        title: 'Name of course 3',
        author: 'author 3',
        year: 1952,
        members: '4.52K',
        mark: 4.53,
    },
    {
        id: '4',
        title: 'Name of course 4',
        author: 'author 4',
        year: 2099,
        members: '4.52K',
        mark: 4.53,
    },
];

const secondCard = {
    title: 'Python',
    author: 'Alexey',
    year: 1995,
    members: '4.12K',
    rating: 4.92,
};

// const cards = [list, secondCard];

interface Props {
    fetchCards: () => Promise<void>;
    pushCard: (title: string, description: string) => Promise<void>;
}

const mapStateToProps = (state: any): {} => ({
    cards: state.cards,
});

const mapDispatchToProps = {
    fetchCards,
    pushCard,
};
// const cards = [list, secondCard];

interface Props {
    fetchCards: () => Promise<void>;
    pushCard: (title: string, description: string) => Promise<void>;
}

class Cards extends React.PureComponent {
    constructor(props: Props) {
        super(props);
        props.fetchCards();
    }
    render() {
        const { cards } = this.props;
        return (
            <Grid size='3'>
                {cards.map((elem) => <Col><Card key={elem.id} properties={elem} /></Col>)}
            </Grid>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Cards);
