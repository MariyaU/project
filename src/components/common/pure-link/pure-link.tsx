import * as React from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';

export const StyledLink = styled(Link)`
    text-decoration: none;
    color: inherit;
    padding-right: 10px;
`;

const PureLink: React.SFC<any> = (props): JSX.Element => {
    const { children, ...rest } = props;

    return (
        <StyledLink {...rest}>
            {children}
        </StyledLink>
    );
};

export default PureLink;
