import styled, { css } from 'styled-components';

const Root = styled.div<{ size }>`
    display: grid;
    ${(props) => css`grid-template-columns: repeat(${props.size}, 1fr);`}
`;

export default Root;
