import styled, { css } from 'styled-components';

const Root = styled.div<{ template: number[], max: number }>`
    display: grid;
    grid-column-gap: 24px;
    ${(props) => css`grid-template-columns: ${props.template.map((size) => `${size}fr `)}`}
`;

export default Root;
