import styled, { css } from 'styled-components';

const Root = styled.div<{ size: string }>`
    ${(props) => css`grid-column: span ${props.size};`}
    margin: auto;
`;

export default Root;