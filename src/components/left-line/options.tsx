import React from 'react';
import { Container, Icon, Title } from './left-line.styles';

const Options = (props) => {
	const properties = props;
	const {title, icon} = properties;
	return (
	<Container>
		<Icon image={icon}/>
		<Title>{title}</Title>
	</Container>
)
}

export default Options;