import styled from 'styled-components';

export const Root = styled.div`
display: flex;
padding-top: 80px;
width: 240px;
height: 100%;
position: fixed;
background: #375079;
flex-direction: column;
align-items:center;
`;

export const SeparateLine = styled.div`
height: 1px;
width: 200px;
background: white;
margin:20px 0;
`;

export const Container = styled.div`
width: 200px;
display: flex;
height: 45px;
align-items: center;
font-family: Roboto;
font-size: 20px;
font-weight: 12;
`;

export const Title = styled.div`
color: white;
cursor: pointer;
`;

export const Icon = styled.div`
width: 32px;
height: 32px;
margin-right: 16px;
background-image: url('${(props) => props.image}');
cursor: pointer;
`;

export const IconName = styled.div`
width: 44px;
height: 44px;
margin-right: 16px;
background-image: url('${(props) => props.image}');
cursor: pointer;
`;

export const Name = styled.div`
color: white;
cursor: pointer;
`;