import React from 'react';
import { Container, Name, IconName } from './left-line.styles';

const Profile = (props) => {
	const properties = props;
	const {title, icon} = properties;
	return (
	<Container>
		<IconName image={icon}/>
		<Name>{title}</Name>
	</Container>
)
}

export default Profile;