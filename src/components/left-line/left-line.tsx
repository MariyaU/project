import React from 'react';
import books from './resourses/books.svg';
import homework from './resourses/my homework.svg';
import settings from './resourses/settings.svg';
import news from './resourses/wats new.svg';
import ava from './resourses/ava.svg';
import { Root, SeparateLine} from './left-line.styles';
import Options from './options';
import Profile from './profile';


const userName = "Masha Ugleva";

const LeftLine = () => {
	return (
		<Root>
			<Profile title={userName} icon={ava}/>
			<SeparateLine />
			<Options title='My coursers' icon= {books}/>
			<Options title='My homeworks' icon={homework} />
			<Options title='Settings' icon={settings} />
			<Options title='Wats new' icon={news} />
		</Root>
	)
}

export default LeftLine;