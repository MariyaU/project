import React from 'react';
import PureLink from '../common/pure-link';

interface Props {
    title: string,
    href: string,
}

const Item = (props: Props) => {
    return (
        <>
            <PureLink to={props.href}>
                {props.title}
            </PureLink>
        </>
    );
};

export default Item;
