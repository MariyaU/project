import React from 'react';
import Item from './item';

interface Item {
    title: string,
    href: string,
    slug: string,
}

const SidebarItems: Item[] = [
    { title: 'Главная', href: '/', slug: 'main' },
    { title: 'Карточки', href: '/cards', slug: 'cards' },
];

const Sidebar = () => {
    return (
        <>
            {SidebarItems.map((item) => <Item key={item.slug} title={item.title} href={item.href} />)}
        </>
    );
};

export default Sidebar;
