import styled from 'styled-components';

export const Root = styled.div`
  width: 205px;
  height: 220px;
  display: flex;
  flex-direction: column;
  background: rgba(196, 196, 196, 0.3);
  align-items: center;
  justify-content: space-between;
  margin: 0 auto;
  border-radius: 2px;
  margin: 12px;
`;

export const Image = styled.div`
    width: 100%;
    height: 110px;
    display: flex;
    background-size: contain;
    border-radius: 2px 2px 0 0;
    background-image: url('${(props) => props.image}');
  `;

export const Information = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: space-between;
    width: 90%;
    height: 95px;
    padding-bottom: 5px;
    text-align: center;
  `;

export const Marks = styled.div`
    display: flex;
    justify-content: space-between;
    width: 90%;
  `;

export const Icon = styled.div`
    height: 15px;
    width: 15px;
    margin: 0 3px;
    background-image: url('${(props) => props.name}');
`;

export const BottomLine = styled.div`
    display: flex;
`;