import React from 'react';
import rating from './resources/rating.svg';
import js from './resources/js.png';
import man from './resources/man-user.svg';
import { Root, Image, Information, Marks, Icon, BottomLine } from './card.styles';

interface Card {
    id: string;
    title: string,
    author: string,
    year: number,
    members: string,
    mark: number,
}

interface Props {
    properties: Card,
}

const Card = (props: Props) => {
    const { properties } = props;
    const { author, title, year, members, mark } = properties;
    return (
        <Root>
            <Image image={''} />
            <Information title='name of cource'>
                <div>
                    <b>{title}</b>
                    <hr />
                </div>
                <div>
                    {author}
                    {year}
                </div>
                <Marks>
                    <BottomLine>
                        <Icon name={man} />
                        {members}
                    </BottomLine>
                    <BottomLine>
                        <Icon name={rating} />
                        {mark}
                    </BottomLine>
                </Marks>
            </Information>
        </Root>
    );
};

export default Card;
