import styled from 'styled-components';

export const Root = styled.div`
display: flex;
z-index: 10;
width: 100%;
height: 60px;
background: #34486A;
position: fixed;
justify-content: space-between;
`;

export const LogoBlock = styled.div`
display: flex;
height: 100%;
align-items: center;
margin-left: 15px;
margin-right: 10px;
`;

export const Logo = styled.div`
width: 40px;
height: 40px;
background-image: url('${(props) => props.image}');
cursor: pointer;
`;

export const Name = styled.div`
display: flex;
align-items: center;
font-family: Roboto;
font-size: 25px;
color: white;
margin-right: 100px;
font-weight: 12;
cursor: pointer;
`;

export const Navigate = styled.div`
display: flex;
align-items: center;
font-family: Roboto;
font-size: 20px;
color: white;
margin-right: 20px;
font-weight: 12;
cursor: pointer;
`;

export const Ava = styled.div`
width: 30px;
height: 30px;
border-radius: 15px;
background: white;
`;

export const Arrow = styled.div`
width: 16px;
height: 16px;
margin-right: 24px;
margin-left: 12px;
background-image: url('${(props) => props.image}');
background-size: contain;
cursor: pointer;
`;

export const Container = styled.div`
display: flex;
align-items: center;
`;

export const Search = styled.input`
width: 230px;
height: 24px;
border-radius: 18px;
border-style: none;
outline: none;
font-family: Roboto;
font-size: 20px;
padding-left: 10px;
font-weight: 12;
/* background-image: url('${(props) => props.image}'); */
/* background-repeat: no-repeat; */
/* background-size: contain;  */
`;