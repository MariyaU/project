import React from 'react';
import { Logo, Root, Name, LogoBlock, Navigate, Ava, Arrow, Container, Search } from './topline.styles';
import logo from './resources.topline/reactlogo.svg';
import arrow from './resources.topline/arrow.svg';
// import lupa from './resources.topline/lupa.svg';

interface Topline {

}

const Topline = () => {
    return (
        <Root>
            <Container>
                <LogoBlock>
                    <Logo image={logo} />
                </LogoBlock>
                <Name>
                    UppErr
                </Name>
                <Navigate>
                    My courses
                </Navigate>
                <Navigate>
                    Homework
                </Navigate>
                <Search placeholder='Search' />
            </Container>
            <Container>
                <Navigate>
                    Name Surname
                </Navigate>
                <Ava />
                <Arrow image={arrow} />
            </Container>
        </Root>
    );
};

export default Topline;