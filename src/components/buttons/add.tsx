import React from 'react';
import styled from 'styled-components';

export const Wrapper = styled.div`
  background: #ffffff;
  position: relative;
  margin: 0 0 0 auto;
  padding: 5px;
  box-shadow: 0 0 1px #000000;
  cursor: pointer;
`;

const Add = ({ onClick }) => {
    return (
        <Wrapper onClick={onClick}>
            Добавить
        </Wrapper>
    );
};
export default Add;
