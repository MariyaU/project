import React from 'react';
import Sidebar from '../sidebar/sidebar';
import Topline from '../topline/topline';
import LeftLine from '../left-line/left-line';
import styled from 'styled-components';

const Wrapper = styled.div`
padding-top: 110px;
margin-left: 240px;
`;

const main = (Component) => () => {
    return (
        <div>
            <Topline />
            <LeftLine />
            <Wrapper>
                <Component />
            </Wrapper>
        </div>
    );
};

export default main;
