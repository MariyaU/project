import styled from 'styled-components';

export const Root = styled.div`
height: calc(100vh - 60px);
display: flex;
align-items: center;
background-size: cover;
justify-content: center;
background: rgba(255, 255, 255, 1);

`;

export const Container = styled.div`
display: flex;
flex-direction: column;
align-items: center;
height: 450px;
width: 400px;
padding-top: 50px; 
background-size: cover;
backdrop-filter: blur(4px);
border-radius: 12px;
background: rgba(0, 0, 0, 0.15);

`;

export const DataMember = styled.div`
display: flex;
flex-direction: column;
align-items: center;
margin: 0 auto;
height: 67px;
width: 400px;
`;

export const Input = styled.input`
height: 30px;
width: 310px;
padding: 0 5px;
border-style: none;
font-size: 14px;
border-radius: 4px;
background: rgba(17, 15, 15, 0.2);
outline: none;
`;

export const Title = styled.div`
font-family: Roboto;
font-style: normal;
font-weight: normal;
font-size: 14px;
line-height: 24px;
color: rgba(0, 0, 0, 0.5);
outline: none;
`;

export const Help = styled.div`
width: 320px;
`;

export const Button = styled.button`
    width: 320px;
    height: 34px;
    font-size: 18px;
    background-color: #34486A;
    border-radius: 4px;
    border-style: none;
    color: white;
    padding: 0;
    margin: 24px 0;
    outline: none;
`;

export const TopLine = styled.div`
width: 320px;
display: flex;
`;

export const SignIn = styled.div`
cursor: pointer;
font-size: 16px;
width: 60px;
margin-right: 32px;
height: 30px;
color: rgba(0, 0, 0, 0.5);
&:hover {
color: rgba(0, 0, 0, 1);
}
`;

export const SignUp = styled.div`
font-size: 16px;
width: 66px;
height: 30px;
color: rgba(0, 0, 0, 0.5);
    &:hover {
        color: rgba(0, 0, 0, 1);
    }
cursor: pointer;
`;

export const Hr = styled.div`
width: 320px;
height: 1px;
background-color: #34486A;
border: none;
margin-bottom: 16px;
`;
