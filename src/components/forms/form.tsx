import React from 'react';
import Fields from './field';
import { Root } from './form.styles';

const Form = () => {
    return (
        <Root>
            <Fields view='up' />
        </Root>
    );
};

export default Form;
