import React from 'react';
import { DataMember, Input, Help, Title, Container, Button, TopLine, SignIn, SignUp, Hr } from './form.styles';

const signUp = ['Full name', 'E-mail', 'Password', 'Confirm password'];
const signIn = ['E-mail', 'Password'];

const fu = (elem) => {
    return (
        <DataMember>
            <Help>
                <Title>{elem}</Title>
            </Help>
            <Input />
        </DataMember>
    );
};

const Fields = (props) => {
    let sign;
    let button;
    const { view } = props;
    if (view === 'in') {
        sign = signIn;
        button = 'SIGN IN';
    } else {
        sign = signUp;
        button = 'SIGN UP';
    }

    return (
        <Container>
            <TopLine>
                <SignIn>SIGN IN</SignIn>
                <SignUp>SIGN UP</SignUp>
            </TopLine>
            <Hr />
            {sign.map((elem) => fu(elem))}
            <Button>
                {button}
            </Button>
        </Container>
    );
};

export default Fields;
